<?php
/*
 * Microblogs plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2009,2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Microblogs plugin for Wolf CMS.
 *
 * The Microblogs plugin for Wolf CMS is made available under the terms of the
 * GNU GPLv3 license. See license.txt
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/
?>
<h1><?php echo __('Settings'); ?></h1>

<form action="<?php echo get_url('plugin/microblogs/save'); ?>" method="post">
    <fieldset style="padding: 0.5em;">
        <legend style="padding: 0em 0.5em 0em 0.5em; font-weight: bold;"><?php echo __('General settings'); ?></legend>
        <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="label"><label for="settings[parent]"><?php echo __('Parent slug'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[parent]" maxlength="255" name="settings[parent]" size="255" type="text" value="<?php echo $settings['parent']; ?>" /></td>
                <td class="help"><?php echo __('Slug of parent page whose new children we want to twitter about.'); ?></td>
            </tr>
            <tr>
                <td class="label"><label for="settings[shortner]"><?php echo __('URL shortner'); ?>: </label></td>
                <td class="field">
                    <select class="select" name="settings[shortner]" id="settings[shortner]">
                        <option value="tinyurl" <?php if ($settings['shortner'] == "tinyurl") echo 'selected ="";' ?>><?php echo __('TinyURL'); ?></option>
                        <option value="bitly" <?php if ($settings['shortner'] == "bitly") echo 'selected ="";' ?>><?php echo __('Bit.ly'); ?></option>
                    </select>
                </td>
                <td class="help"><?php echo __('What URL Shortner service do you want to use?'); ?></td>
            </tr>
        </table>
    </fieldset>

    <fieldset style="padding: 0.5em;">
        <legend style="padding: 0em 0.5em 0em 0.5em; font-weight: bold;"><?php echo __('Twitter settings'); ?></legend>
        <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="label"><label for="settings[consumer_key]"><?php echo __('Consumer key'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[consumer_key]" maxlength="255" name="settings[consumer_key]" size="255" type="text" value="<?php echo $settings['consumer_key']; ?>" /></td>
                <td class="help"><?php echo __('Twitter consumer key.'); ?></td>
            </tr>
            <tr>
                <td class="label"><label for="settings[consumer_secret]"><?php echo __('Consumer secret'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[consumer_secret]" maxlength="255" name="settings[consumer_secret]" size="255" type="text" value="<?php echo $settings['consumer_secret']; ?>" /></td>
                <td class="help"><?php echo __('Twitter consumer secret.'); ?></td>
            </tr>
            <tr>
                <td class="label"><label for="settings[user_token]"><?php echo __('User token'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[user_token]" maxlength="255" name="settings[user_token]" size="255" type="text" value="<?php echo $settings['user_token']; ?>" /></td>
                <td class="help"><?php echo __('Twitter user token.'); ?></td>
            </tr>
            <tr>
                <td class="label"><label for="settings[user_secret]"><?php echo __('User secret'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[user_secret]" maxlength="255" name="settings[user_secret]" size="255" type="text" value="<?php echo $settings['user_secret']; ?>" /></td>
                <td class="help"><?php echo __('Twitter user secret.'); ?></td>
            </tr>
        </table>
    </fieldset>

    <fieldset style="padding: 0.5em;">
        <legend style="padding: 0em 0.5em 0em 0.5em; font-weight: bold;"><?php echo __('Bit.ly / J.mp settings'); ?></legend>
        <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="label"><label for="settings[jmp_login]"><?php echo __('Bit.ly user id'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[jmp_login]" maxlength="255" name="settings[jmp_login]" size="255" type="text" value="<?php echo $settings['jmp_login']; ?>" /></td>
                <td class="help"><?php echo __('Bit.ly user id.'); ?></td>
            </tr>
            <tr>
                <td class="label"><label for="settings[jmp_apikey]"><?php echo __('Bit.ly API key'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[jmp_apikey]" maxlength="255" name="settings[jmp_apikey]" size="255" type="text" value="<?php echo $settings['jmp_apikey']; ?>" /></td>
                <td class="help"><?php echo __('Bit.ly API key.'); ?></td>
            </tr>
            <tr>
                <td class="label"><label for="settings[jmp_domain]"><?php echo __('Preferred domain'); ?>: </label></td>
                <td class="field">
                    <select class="select" name="settings[jmp_domain]" id="settings[jmp_domain]">
                        <option value="j.mp" <?php if ($settings['jmp_domain'] == "j.mp") echo 'selected ="";' ?>>http://j.mp</option>
                        <option value="bit.ly" <?php if ($settings['jmp_domain'] == "bit.ly") echo 'selected ="";' ?>>http://bit.ly</option>
                    </select>
                </td>
                <td class="help"><?php echo __('What domain to use for shortened URLs, e.g. bit.ly or j.mp.'); ?></td>
            </tr>
        </table>
    </fieldset>
    
    <p class="buttons">
        <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
        or <a href="<?php echo get_url('/plugin/microblogs'); ?>"><?php echo __('cancel'); ?></a>
    </p>
</form>