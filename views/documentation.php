<?php
/*
 * Microblogs plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2009,2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Microblogs plugin for Wolf CMS.
 *
 * The Microblogs plugin for Wolf CMS is made available under the terms of the
 * GNU GPLv3 license. See license.txt
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/
?>
<h1><?php echo __('Documentation'); ?></h1>
<h2><?php echo __('Configuring twitter authentication'); ?></h2>
<ol style="list-style-position: inside; list-style-type: 1;">
    <li>If you don't have one already, create a Twitter application for your site on http://dev.twitter.com/apps
        <ul style="margin-left: 1.5em; list-style-position: inside; list-style-type: disc;">
            <li>Login using the twitter account you want to use with the plugin.</li>
            <li>Register a new app.</li>
            <li>For "Application Type", select "Client".</li>
            <li>For "Default Access Type", select "Read & Write"</li>
        </ul>
    </li>
    <li>From the application details page copy the consumer key and consumer secret
   into the settings page of the plugin.</li>
    <li>Visit the 'My Access Token' screen linked to from your application details page.</li>
    <li>Copy the user token and user secret into the settings page of the plugin.</li>
    <li>Save the settings and try adding a new page, the plugin should now tweet
        about the new page using the twitter account for which you created the
        application on dev.twitter.com
    </li>
</ol>
<h2><?php echo __('Configuring the Bit.ly URL shortner'); ?></h2>
<ol style="list-style-position: inside; list-style-type: 1;">
    <li>If you don't have one already, create a Bit.ly user account.</li>
    <li>On the plugin's settings page, enter your Bit.ly user id and API key.
        <ul style="margin-left: 1.5em; list-style-position: inside; list-style-type: disc;">
            <li>The API key is mentioned on your Bit.ly settings page. (http://bit.ly/a/account)</li>
        </ul>
    </li>
    <li>Select "Bit.ly" for the URL shortner setting in the plugin's "General settings" section.</li>
</ol>
<h2>Notes</h2>
<p>
    This plugin currently only tweets about a page you ADDED when the page's status was set to
    PUBLISHED at the time it was first added.
</p>
<p>It does not currently tweet about pages you updated.</p>
<p>For "Application Name" I suggest you use "Example.com" and for
   "Application Website" I suggest you use "http://www.example.com". This will
   give you a nice "via" link on the Twitter site.
</p>