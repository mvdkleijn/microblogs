<?php
/*
 * Microblogs plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2009,2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Microblogs plugin for Wolf CMS.
 *
 * The Microblogs plugin for Wolf CMS is made available under the terms of the
 * GNU GPLv3 license. See license.txt
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/
?>
<p class="button">
    <a href="<?php echo get_url('plugin/microblogs/documentation'); ?>">
        <img src="<?php echo URL_PUBLIC; ?>wolf/plugins/microblogs/images/documentation.png" align="middle" alt="documentation icon" />
        <?php echo __('Documentation'); ?>
    </a>
</p>
<p class="button">
    <a href="<?php echo get_url('plugin/microblogs/settings'); ?>">
        <img src="<?php echo URL_PUBLIC; ?>wolf/plugins/microblogs/images/settings.png" align="middle" alt="settings icon" />
        <?php echo __('Settings'); ?>
    </a>
</p>

<div class="box">
    <h2><?php echo __('About Microblogs'); ?></h2>
    <p>
        <?php echo __('The microblogs plugin posts titles of new articles to Twitter.'); ?>
    </p>
    <p>
        <?php echo __('Homepage'); ?>: <a href="http://www.vanderkleijn.net/">vanderkleijn.net</a>
    </p>
</div>