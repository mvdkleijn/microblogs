<?php
/*
 * Microblogs plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2009,2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Microblogs plugin for Wolf CMS.
 *
 * The Microblogs plugin for Wolf CMS is made available under the terms of the
 * GNU GPLv3 license. See license.txt
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/

/**
 * The microblogs plugin posts the title of new articles to twitter along
 * with a tinyurl or bit.ly url to the article.
 *
 * The intention is to expand this plugin to include other microblogging services
 * such as Identi.ca and other URL shortners like is.gd.
 *
 * @package wolf
 * @subpackage plugin.microblogs
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @since Wolf CMS version 0.6.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2009,2010
 */

Plugin::setInfos(array(
        'id'          => 'microblogs',
        'title'       => 'Microblogs',
        'description' => 'Posts titles of new articles to Twitter.',
        'version'     => '0.7.5',
        'license'     => 'GPLv3',
        'author'      => 'Martijn van der Kleijn',
        'website'     => 'http://vanderkleijn.net/wolf-cms/plugins/microblogs.html',
        'update_url'  => 'http://www.vanderkleijn.net/plugins.xml',
        'require_wolf_version' => '0.6.0'
));

if (defined('CMS_BACKEND')) {

// Commented out for now as we don't need it
if (CMS_VERSION < '0.7.0') {
    Plugin::addController('microblogs', 'Microblogs', 'administrator', false);
}
else {
    Plugin::addController('microblogs', 'Microblogs', 'admin_view', false);
}

// We observe the page_add_after_save so in essence, we only post to twitter when
// a page is first added. Not on updates of the page. If the page status isn't set
// to published at the time of adding the page to Wolf, the page isn't twittered
// about at all.
Observer::observe('page_add_after_save', 'postToTwitter');

function postToTwitter($page) {
    // Get settings
    $settings = Plugin::getAllSettings('microblogs');

    $parent = $page->findById($page->parent_id);

    // Check if we need to tweet for this page
    // Change this if you want to post pages published in archive.
    if ($parent->slug != $settings['parent'] || $page->status_id != Page::STATUS_PUBLISHED) {
        return;
    }
    
    require 'tmhOAuth.php';
    
    $tmhOAuth = new tmhOAuth(array(
        'consumer_key'    => $settings['consumer_key'],
        'consumer_secret' => $settings['consumer_secret'],
        'user_token'      => $settings['user_token'],
        'user_secret'     => $settings['user_secret'],
    ));

    $title = $page->title;

    // @todo Improve url creation mechanism since it doesn't work for Archive plugin.
    if (!isset($page->parent) || ($page->parent === false || empty($page->parent))) {
        $page->parent = Page::findById($page->parent_id);
        $page->url = trim($page->parent->url .'/'. $page->slug, '/');
    }

    // We will want to seperate this into another function all together.
    $url = URL_PUBLIC . (endsWith(URL_PUBLIC, '/') ? '': '/') . (USE_MOD_REWRITE ? '': '?') . $settings['parent'].'/' . $page->slug;
    $surl = shorten($url);

    // Check the url was shortened, if not, abort
    if (false === $surl) {
        return;
    }
    
    $shortened = ' ('.$surl.')';
    
    if (strlen($title) > (139-strlen($shortened))) {
        $title = substr($title,0,strlen($shortened)-4).'...';
    }

    $status = $title.$shortened;
    
    $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
        'status' => $status
    ));

    if ($tmhOAuth->response['code'] == 200) {
        //$tmhOAuth->pr(json_decode($tmhOAuth->response['response']));
        //Flash::set('success', $value)
    } else {
        Flash::set('error', __('Microblogs - An error occurred while attempting to tweet.'));
        if (defined('DEBUG')) {
            throw new Exception($tmhOAuth->pr(htmlentities($tmhOAuth->response['response'])));
        }
    }
}


function shorten($url) {
    $settings = Plugin::getAllSettings('microblogs');

    if ($settings['shortner'] == 'tinyurl') {
        $result = file_get_contents('http://tinyurl.com/api-create.php?url='.$url.'.html');
        return $result;
    }
    if ($settings['shortner'] == 'bitly') {
        $url_enc = urlencode($url);

        if (function_exists('json_decode')) {
            $format = 'json';
        }
        else {
            $format = 'txt';
        }

        $jmp = 'http://api.bit.ly/v3/shorten?login='.$settings['jmp_login'].'&apiKey='.$settings['jmp_apikey'].'&longUrl='.$url_enc.'&domain='.$settings['jmp_domain'].'&format='.$format;
        $result = file_get_contents($jmp);

        if (function_exists('json_decode')) {
            $result = json_decode($result);

            if ($result->status_code == '200' && $result->status_txt == 'OK') {
                $data = $result->data;
                return $data->url;
            }
            else if ($result->status_code == '403' && $result->status_txt == 'RATE_LIMIT_EXCEEDED') {
                Flash::set('error', __('Microblogs - Bit.ly says you have exceeded their rate limit.'));
            }

            return false;
        }
        else {
            return $result;
        }
    }
}


}