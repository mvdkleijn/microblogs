== WHAT IT IS ==

* a WolfCMS-to-microblog-service plugin for Wolf CMS.

== HOW TO USE IT ==

* Enable the plugin.
* Check and update the settings for the plugin. (see below for more details)

=== Configuring twitter authentication ===

1) If you don't have one already, create a Twitter application for your site on
   http://dev.twitter.com/apps

    - Login using the twitter account you want to use with the plugin.
    - Register a new app.
    - For "Application Type", select "Client".
    - For "Default Access Type", select "Read & Write"

2) From the application details page copy the consumer key and consumer secret
   into the settings page of the plugin.

3) Visit the 'My Access Token' screen linked to from your application details
   page.

4) Copy the user token and user secret into the settings page of the plugin.

5) Save the settings and try adding a new page, the plugin should now tweet
   about the new page using the twitter account for which you created the
   application on dev.twitter.com

=== Configuring Bit.ly shortner ===

1) If you don't have one already, create a Bit.ly user account.

2) On the plugin's settings page, enter your Bit.ly user id and API key.

    - The API key is mentioned on your Bit.ly settings page. (http://bit.ly/a/account)

3) Select "Bit.ly" for the URL shortner setting in the plugin's "General settings" section.


== NOTES ==

* Currently it only supports Twitter and only one (configurable) Twitter account.
* Currently it only supports TinyURL.

== LICENSE ==

Copyright 2009-2010, Martijn van der Kleijn. <martijn.niji@gmail.com>
This Microblogs plugin is licensed under the GPLv3 License.
license.txt -or- <http://www.gnu.org/licenses/gpl.html>
