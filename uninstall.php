<?php
/*
 * Microblogs plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Microblogs plugin for Wolf CMS.
 *
 * The Microblogs plugin for Wolf CMS is made available under the terms of the
 * GNU GPLv3 license. See license.txt
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/

/* Security measure */
if (CMS_VERSION > '0.6.0b' && !defined('IN_CMS')) { exit(); }

if (Plugin::deleteAllSettings('microblogs') === false) {
    Flash::set('error', __('Unable to delete plugin settings.'));
    redirect(get_url('setting'));
}

Flash::set('success', __('Successfully uninstalled plugin.'));
redirect(get_url('setting'));
