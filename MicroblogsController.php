<?php
/*
 * Microblogs plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2009,2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Microblogs plugin for Wolf CMS.
 *
 * The Microblogs plugin for Wolf CMS is made available under the terms of the
 * GNU GPLv3 license. See license.txt
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/

/**
 * The microblogs plugin posts the title of new articles to twitter along
 * with a tinyurl to the article.
 *
 * The intention is to expand this plugin to include other microblogging services
 * such as Identi.ca and other URL shortners like bit.ly.
 *
 * @todo Allow user to configure which pages to post. (regex config?)
 * @todo Allow user to configure posting style. (Title+URL, Title, First line+URL, etc.)
 *
 * @package wolf
 * @subpackage plugin.microblogs
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf CMS version 0.6.0
 * @license http://www.gnu.org/licenses/gpl.html GPL License
 * @copyright Martijn van der Kleijn, 2009,2010
 */

class MicroblogsController extends PluginController {

    function __construct() {
        define('MBC_VIEWS_BASE', 'microblogs/views');
        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('../../plugins/microblogs/views/sidebar'));

    }

    private function _checkLoggedIn() {
        AuthUser::load();
        if (!AuthUser::isLoggedIn()) {
            Flash::set('error', __("You don't have permission to access this functionality."));
            redirect();
        }
    }

    public function index() {
        $this->display(MBC_VIEWS_BASE.'/documentation');
    }

    public function documentation() {
        $this->display(MBC_VIEWS_BASE.'/documentation');
    }

    public function settings() {
        $this->_checkLoggedIn();

        $this->display(MBC_VIEWS_BASE.'/settings', array('settings' => Plugin::getAllSettings('microblogs')));
    }

    function save() {
        if (isset($_POST['settings'])) {
            $settings = $_POST['settings'];

            // Sanity checking for settings
            // After trimming all values
            foreach ($settings as $key => $value) {
                $settings[$key] = trim($value);
            }

            if (empty($settings['consumer_key']) || empty($settings['consumer_secret']) ||
                empty($settings['user_token']) || empty($settings['user_secret']) ) {
                Flash::setNow('error', 'Microblogs - You have to fill in all Twitter fields.');
                $this->display(MBC_VIEWS_BASE.'/settings', array('settings' => $settings));
            }

            if ($settings['shortner'] == 'bitly') {
                // Make sure the bit.ly userid & api key are set.
                if (empty($settings['jmp_login']) || empty($settings['jmp_apikey'])) {
                    Flash::setNow('error', 'Microblogs - When using the Bit.ly URL shortner, you have to enter a user account and API key.');
                    $this->display(MBC_VIEWS_BASE.'/settings', array('settings' => $settings));
                }
            }

            foreach ($settings as $key => $value) {
                $settings[$key] = mysql_escape_string($value);
            }

            $ret = Plugin::setAllSettings($settings, 'microblogs');

            if ($ret) {
                Flash::set('success', __('The settings have been saved.'));
            }
            else {
                Flash::set('error', 'An error occured trying to save the settings.');
            }
        }
        else {
            Flash::set('error', 'Could not save settings, no settings found.');
        }

        redirect(get_url('plugin/microblogs/settings'));
    }
}