<?php
/*
 * Microblogs plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Microblogs plugin for Wolf CMS.
 *
 * The Microblogs plugin for Wolf CMS is made available under the terms of the
 * GNU GPLv3 license. See license.txt
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/

// Security measure
if (CMS_VERSION > '0.6.0b' && !defined('IN_CMS')) { exit(); }

// Check if the plugin's settings already exist and create them if not.
if (Plugin::getSetting('version', 'microblogs') === false) {
    $settings = array('version'         => '0.7.0',
                      'parent'          => 'articles',
                      'shortner'        => 'tinyurl',
                      'consumer_key'    => '',
                      'consumer_secret' => '',
                      'user_token'      => '',
                      'user_secret'     => '',
                      'jmp_login'       => '',
                      'jmp_apikey'      => '',
                      'jmp_domain'      => 'j.mp'
                     );

    Plugin::setAllSettings($settings, 'microblogs');
}
